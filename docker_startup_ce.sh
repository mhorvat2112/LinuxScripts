#!/bin/bash

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#PRIJE IZVRSAVANJA SKRIPTE DODATI
#subjectAltName = IP:192.168.56.102
#U /etc/ssl/openssl.cnf POD POLJE [ v3_ca ]
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#instaliranje dockera
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker

docker --version

sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt update
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic test"
sudo apt update
sudo apt install docker-ce

#ovaj dio naredbi samo u slucaju da se radi o setupanju docker registryazz
#konfiguriranje ssl-a i certifikata
sudo nano /etc/ssl/openssl.cnf
sudo mkdir -p /certs
openssl req \-newkey rsa:4096 -nodes -sha256 -keyout /certs/domain.key \-x509 -days 365 -out /certs/domain.crt #pod Commn Name upisati: 192.168.56.102
sudo docker run -d -p 5000:5000 --restart=always --name registry \-v /certs:/certs \-e REGISTRY_HTTP