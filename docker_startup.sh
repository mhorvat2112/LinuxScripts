#!/bin/bash

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
#PRIJE IZVRSAVANJA SKRIPTE DODATI
#subjectAltName = IP:192.168.56.102
#U /etc/ssl/openssl.cnf POD POLJE [ v3_ca ]
#
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#instaliranje dockera
sudo apt install docker.io
sudo systemctl start docker
sudo systemctl enable docker

docker --version

sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt update
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic test"
sudo apt update